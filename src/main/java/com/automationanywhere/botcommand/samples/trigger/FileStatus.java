/*
 * Copyright (c) 2020 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */

package com.automationanywhere.botcommand.samples.trigger;

import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.GreaterThan;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.annotations.rules.NumberInteger;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.core.security.SecureString;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Trigger will check the presence of a flag file on a recurring basis
 * NOTE 1: This class is for illustrative purpose only not safe for use in production.<br>
 *
 * @author Bren Sapience
 *
 **/

@BotCommand(commandType = BotCommand.CommandType.Trigger)
@CommandPkg(label = "File Status Query Trigger", description = "File Status Query Trigger", icon = "pkg.svg", name = "fileQueryTrigger")
public class FileStatus {
	
	private static Logger logger = LogManager.getLogger(FileStatus.class);

	// Map storing multiple timer tasks
	private static final Map<String, TimerTask> taskMap = new ConcurrentHashMap<>();
	private static final Timer TIMER = new Timer(true);

	@TriggerId
	private String triggerUid;
	@TriggerRunnable
	private Runnable runnable;

	@StartListen
	public void startTrigger(
			@Idx(index="1", type = AttributeType.TEXT) @Pkg(label = "Path and Name of Flag File") @NotEmpty String FullFilePath,
			@Idx(index = "2", type = AttributeType.NUMBER) @Pkg(label = "Please provide the interval to query in seconds", default_value = "5", default_value_type = DataType.NUMBER) @GreaterThan("0") @NumberInteger @NotEmpty Double interval)

	{

		// seconds to milliseconds
		interval = interval*1000;

		// The TimerTask is the definition of what condition(s) the Trigger should check
		TimerTask timerTask = new TimerTask() {

			@Override
			public void run() {
				// The following code runs every X Seconds and performs a check...
				File tempFile = new File(FullFilePath);
				logger.debug("checking If File Exists: "+tempFile.getAbsolutePath());
				try {
					// If the check is successul = the condition is met THEN we trigger runnable.run() which effectively triggers the associated Bot
					if(tempFile.exists()){
						logger.debug("File Found!");
						runnable.run();
						return;

					}
					// If the condition is NOT met, then we can do nothing or simply log a message in the Device log
					else{
						logger.debug("File Not Found");
					}
				} catch (Exception e) {
					logger.warn(e.getMessage(),e);
					logger.warn("Trigger is still running.");
				}

			}
		};

		// This registers the Trigger and its task in a Dictionary of existing triggers
		taskMap.put(this.triggerUid, timerTask);

		// This specifies at what intervals the Trigger's logic should run (and therefore how often Conditions should be checked)
		TIMER.schedule(timerTask, interval.longValue(), interval.longValue());
	}

	// You do NOT need to modify the rest of the code for this class!!

	/*
	 * Cancel all the task and clear the map.
	 */
	@StopAllTriggers
	public void stopAllTriggers() {
		taskMap.forEach((k, v) -> {
			if (v.cancel()) {
				taskMap.remove(k);
			}
		});
	}

	/*
	 * Cancel the task and remove from map
	 *
	 * @param triggerUid
	 */
	@StopListen
	public void stopListen(String triggerUid) {
		if (taskMap.get(triggerUid).cancel()) {
			taskMap.remove(triggerUid);
		}
	}
    
	public String getTriggerUid() {
		return triggerUid;
	}

	public void setTriggerUid(String triggerUid) {
		this.triggerUid = triggerUid;
	}

	public Runnable getRunnable() {
		return runnable;
	}

	public void setRunnable(Runnable runnable) {
		this.runnable = runnable;
	}

}
