package com.automationanywhere.botcommand.samples.trigger;

import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.samples.trigger.FileStatus;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.File;

@Test(enabled=true)
public class FileStatusTests {

	public static void main(String[] args){
		String FilePath = "C:\\iqbot\\trigger\\flag.txt";
		Double interval = 5d;
		FileStatus trigger = new FileStatus();
		trigger.startTrigger(FilePath,interval);

	}

}
